reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#filename='./dat/11gev_coul_noq_12-06-2017/11gev_coul_noq_12-06-2017.dat'
filename='./dat/39gev_trackeff6_01-30-2018/39gev_trackeff6_01-30-2018.dat'
KT=2

filename2="./dat/star_data_200gev"

#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'q2dep_fixkt',KT,'.eps')
set out "image.eps"
################
set xrange [-5:79]
set yrange [1.5:6.5]
################
radii_label_y=5.8
energy_label_y=2.4
################
set mytics   5
set mxtics   5
set ytics scale 1.
set xtics scale 1.

#set logscale x
set xtics 20
set ytics 1
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
###############
################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
################
#####DO NOT TOUCH####
set size    0.45,0.50
set origin -0.00,0.48
################

set key samplen  1.2
set key at 5,-6
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'R'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set label "Rside"       at 0,radii_label_y    font "Times-Roman,  18" 

p  (filename) every ::(KT)::(KT) u 2:12:($15*1)  t"TOF" with errorbars lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename2) every ::(KT)::(KT) u 1:4:5    t"Star data"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5
   
################
################
set size    0.45,0.50
set origin 0.30,0.48
unset key
unset label

set key samplen  1.2
set key at -6,radii_label_y
set key spacing 1

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5

  set label "Rout"      at 0,radii_label_y    font "Times-Roman,  18" 
  set label "200 GeV"    at 0,energy_label_y    font "Times-Roman,  16" 
  set label "<Kt>=0.30"       at 0,(energy_label_y-0.5)    font "Times-Roman,  16" 
  set label 'Centrality%'     at 18,(energy_label_y-1.5)    font "Times-Roman,  24" 

p  (filename) every ::(KT)::(KT) u 2:13:($16*1)  t"Coulomb Corrected" with errorbars lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename2) every ::(KT)::(KT) u 1:6:7    t"PRC.92.014904"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5
   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:13:($16*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:13:($16*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:13:($16*1)    t"Q90%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
################
################
set size    0.45,0.50
set origin 0.6,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '


  set label "Rlong"       at 0,radii_label_y    font "Times-Roman,  18" 
p  (filename) every ::(KT)::(KT) u 2:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename2) every ::(KT)::(KT) u 1:8:9    t"Q80%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5
   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:14:($17*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:14:($17*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:14:($17*1)    t"Q90%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2

###############
###############
unset multiplot
################

