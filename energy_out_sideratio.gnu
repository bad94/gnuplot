reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
RPbins=5
cent=4
KT=1  
filename="./dat/39Gev/39gev_11-20-2017.dat"
filename2="./dat/27Gev/27gev_kt2_12-03-2017/27gev_kt2_12-03-2017.dat"
filename3='./dat/200Gev/200gev_testkt_11-28-2017/200gev_testkt_11-28-2017.dat'#"./dat/200gev_11-21-2017.dat"
filename4="./dat/19Gev/19gev_11-28-2017.dat"
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'depkt_fix',cent,'multiq.eps')
set out "image.eps"
################
set xrange [10:298]
set yrange [0.9:1.35]
################
radii_label_y=1.3
energy_label_y=1
##############
set mytics   5
set mxtics   5
set ytics scale 1.3
set xtics scale 1.
##############
##change here###
set logscale x
set xtics 10
set ytics 0.2
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
#########################
#########################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
#########################
#####DO NOT TOUCH########
set size    0.45,0.50
set origin -0.00,0.48
################
set key samplen  1.2
set key at 0.59,radii_label_y
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'Ratio Q_{2} (low/ high)'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set label "Rside"       at 13,(radii_label_y)    font "Times-Roman,  18" 
set arrow 2 from 10,1 to 298,1 nohead lt 0 lw 4 lc 0
################
  p (filename)   every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u ($5*0+200):($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13))) t"200Gev" with errorbars  lt 1 lw 2 lc 3  pt 8  ps 2,\
    (filename2)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u ($5*0+39):($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13)))  t"39Gev"  with errorbars  lt 1 lw 2 lc 3  pt 8  ps 2,\
    (filename3)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u ($5*0+27):($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13))) t"27Gev"  with errorbars  lt 1 lw 2  lc 3  pt 8  ps 2,\
    (filename4)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u ($5*0+19):($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13)))  t"19Gev"  with errorbars  lt 1 lw 2 lc 3  pt 8  ps 2,\
    (filename)   every ::(KT*RPbins+4):(cent):(KT*RPbins+4):(cent) u ($5*0+200):($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13))) with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
    (filename2)  every ::(KT*RPbins+4):(cent):(KT*RPbins+4):(cent) u ($5*0+39):($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13)))  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
    (filename3)  every ::(KT*RPbins+4):(cent):(KT*RPbins+4):(cent) u ($5*0+27):($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13)))  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
    (filename4)  every ::(KT*RPbins+4):(cent):(KT*RPbins+4):(cent) u ($5*0+19):($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13)))  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
################
set size    0.45,0.50
set origin 0.26,0.48
unset key
unset label
#set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
set ylabel " "

  set label "Rout"      at 13,(radii_label_y)   font "Times-Roman,  18" 
  #set label "39 GeV"    at 0.16,(energy_label_y)    font "Times-Roman,  18" 
  # 0-5(0),  5-10(1),10-20(2),20-30(3),30-40(4),40-50(5),50-60(6),60-70(7)
  cent_label=sprintf("Centrality=%i%s",cent*10,'%')
  set label (cent_label)       at 20,(energy_label_y-.05)   font "Times-Roman,  18" 
  set label '{/Symbol @{\140\140\140\140}\326} S_{NN}'     at 30,(energy_label_y-0.2)    font "Times-Roman,  24" 
set arrow 2 from 10,1 to 298,1 nohead lt 0 lw 4 lc 0
   p (filename)  every ::(KT*RPbins+4):(cent):(KT*RPbins+4):(cent)  u ($5*0+200):19:($22*1) t"200Gev" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
    (filename2)  every ::(KT*RPbins+4):(cent):(KT*RPbins+4):(cent) u ($5*0+39) :19:($22*1)  t"39Gev"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
    (filename3)  every ::(KT*RPbins+4):(cent):(KT*RPbins+4):(cent) u ($5*0+27) :19:($22*1)  t"27Gev"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
    (filename4)  every ::(KT*RPbins+4):(cent):(KT*RPbins+4):(cent) u ($5*0+19) :19:($22*1)  t"27Gev"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\