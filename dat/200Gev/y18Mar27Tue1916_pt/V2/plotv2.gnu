reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
filename1='./c22_9'
filename2='./c22_8'
filename22='c22_7'
filename3='./c22_10'
filename42='c22_2'
filename4='./c22_1'
filename5='./c22_0'
filenameN='200_niseem_3-27'
set out "V2.eps"
################kt=[0.15:0.65]
set xrange [0:650]
set yrange [0:0.1]
################
key_label=0.098
energy_label=3.3
kt_label = 1.3
######DONT CHANGE##########
set mytics   5
set mxtics   5
set ytics scale 1.
set xtics scale 1.

#set logscale x
#below is scale for axis tics
set xtics 150
set ytics 0.02
################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
################
#####DO NOT TOUCH####
set size    0.45,0.50
set origin -0.00,0.48
################

set key samplen  1.2
set key at 630,key_label
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  10"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  15"
set xlabel font "Times-Roman,  15" 
set ylabel font "Times-Roman,  15"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'V_{2}'
set xlabel 'Multiplicity'
set ytics  offset 0.7,0.0
set ylabel offset 4.5,0.0
set xlabel offset 0,1

#set label "Rside"       at 0.16,radii_label    font "Times-Roman,  18"
#p   (filename3) u 1:2:3 t"NoQ_2"   with errorbars lt 1 lw 2 lc 8  pt 6  ps 1 ,\
#    (filenameN) u 1:2:3 t"niseem"  with errorbars lt 1 lw 2 lc 7  pt 5  ps 1 ,\


p (filename1) u 1:2:3 t"90-100"  with errorbars lt 1 lw 2 lc 9  pt 11  ps 1 ,\
  (filename2) u 1:2:3 t"80-90%" with errorbars lt 1 lw 2 lc 7  pt 5  ps 1 ,\
 (filename22) u 1:2:3 t"70-80%" with errorbars lt 1 lw 2 lc 4  pt 2  ps 1 ,\
  (filename3) u 1:2:3 t"NoQ_2"  with errorbars lt 1 lw 2 lc 8  pt 6  ps 1 ,\
  (filename42) u 1:2:3 t"20-30" with errorbars lt 1 lw 2 lc 1  pt 15  ps 1 ,\
  (filename4) u 1:2:3 t"10-20%" with errorbars lt 1 lw 2 lc 2  pt 9  ps 1 ,\
  (filename5) u 1:2:3 t"0-10%"  with errorbars lt 1 lw 2 lc 6  pt 7  ps 1,\
  (filenameN) u 1:2:3 t"niseem"  with errorbars lt 1 lw 2 lc 7  pt 4  ps 1