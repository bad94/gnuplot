reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'q2dep_fixkt',KT,'.eps')
set out "image.eps"
################
set xrange [0.005:0.25]
set yrange [0.87:1.6]
################
set mytics   5
set mxtics   5
set ytics scale 1.0
set xtics scale 1.0
#set logscale x
#CHANGE HERE
set xtics 0.1
set ytics 0.2
#####################
#####################
set multiplot layout 0,2
#set xtics rotate
#set bmargin 5
#####################
#####################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
#####################
###DO NOT TOUCH######
set size    0.4,0.4
set origin -0.0,0.48
#####################
set key samplen  1.2
set key at 0.2,1.48
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"
#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0  ,0.5
set ytics  offset 0.1,0.0
set ylabel offset 2.9,0.0
#THESE ARE FOR SECECTION ONLY not changing the # of bins.
# cent goes from 0,1,2 for 19gev and kt goes from 0..3 and rpgoes from 0..4
#Rside=0,Rout=1,Rlong=2
set xtics  offset 0,0.5
set ylabel 'C(q)'
set ytics  offset 0.8,0.0
set ylabel offset 4,0.0
#name='19gev_fullq_02-06-2018'
RType="side"
name='200gev_starmerge2_02-14-2018/'
filename=sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
Rlabel = sprintf("%s%s",'R',RType)
#set label "Rside"       at 0.03,1.4    font "Times-Roman,  18" 
#set label "19Gev"       at .95,1.4     font "Times-Roman,  18"
cent=0
KT=0
RPbin=10
qselection=1
#Do not forget to remove uncessary spaces
#corr_block=(cent*5)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*11)+RPbin; else corr_block = (cent*4)+KT;
#corr_block = (cent*16)+(KT*4)+RPbin #rpbin=4
#corr_block = (cent*16)+(KT*4)+RPbin
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1

################################################
################################################
set size    0.4,0.4
set origin 0.21,0.48
unset key
unset label

set key samplen  1.2
set key at 1.4,-5
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT ;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1
################
################
set size    0.4,0.4
set origin 0.42,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1

#################################
################
set size    0.4,0.4
set origin 0.63,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT ;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1
# #################################
# ################
# set size    0.45,0.50
# set origin 0.3,0.0

# unset label
# unset key


# set key samplen  1.2
# set key at 1.4,-5
# set key spacing 0.5

# set key    font "Times-Roman,  13"
# set xtics  font "Times-Roman,  15" 
# set ytics  font "Times-Roman,  1"
# set xlabel font "Times-Roman,  1" 
# set ylabel font "Times-Roman,  1"
# #set xlabel ' '
# RPbin=4
# #Do not forget to remove uncessary spaces
# corr_block = (cent*20)+(KT*5)+RPbin
# set label Rlabel       at 0.15,1.18    font "Times-Roman,  18"
# #set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
# #set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
# #set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
# #set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
# p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
#    (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2 lc 1

# ###############
# unset multiplot
# ################


################################################
################################################
#################ROUT###########################
################################################
################################################################################################
###############################################
set size    0.4,0.4
set origin -0.0,0.233
#####################
unset label
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"
set ylabel 'C(q)'
RType="out"
filename=sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
Rlabel = sprintf("%s%s",'R',RType)
#set label "Rside"       at 0.03,1.4    font "Times-Roman,  18" 
#set label "19Gev"       at .95,1.4     font "Times-Roman,  18"
KT=0
#Do not forget to remove uncessary spaces
#corr_block=(cent*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin #rpbin=4
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin ; else corr_block = (cent*4)+KT;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1
 #############################################
 ####################################################
 ##############################################
set size    0.4,0.4
set origin 0.21,0.233
unset key
unset label

set key samplen  1.2
set key at 1.4,-5
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1
################
################
set size    0.4,0.4
set origin 0.42,0.233

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1

#################################
################
set size    0.4,0.4
set origin 0.63,0.233

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT ;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1

   ################################################
################################################
#################RLONG###########################
################################################
################################################################################################
###############################################
set size    0.4,0.4
set origin -0.0,-0.015
#####################
unset label
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"
set ylabel 'C(q)'
RType="long"
filename=sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
Rlabel = sprintf("%s%s",'R',RType)
#set label "Rside"       at 0.03,1.4    font "Times-Roman,  18" 
#set label "19Gev"       at .95,1.4     font "Times-Roman,  18"
KT=0
#Do not forget to remove uncessary spaces
#corr_block=(cent*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin #rpbin=4
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin ; else corr_block = (cent*4)+KT;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1
 #############################################
 ####################################################
 ##############################################
set size    0.4,0.4
set origin 0.21,-0.015
unset key
unset label

set key samplen  1.2
set key at 1.4,-5
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT ;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1
################
################
set size    0.4,0.4
set origin 0.42,-0.015

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin ; else corr_block = (cent*4)+KT;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1

#################################
################
set size    0.4,0.4
set origin 0.63,-0.015

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
#if(qselection) corr_block = (cent*20)+(KT*5)+RPbin ; else corr_block = (cent*4)+KT ;
#if(qselection) corr_block = (cent*24)+(KT*6)+RPbin; else corr_block = (cent*4)+KT;
if(qselection) corr_block = (cent*44)+(KT*10)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc (RPbin+0)  pt 8  ps 1,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"3DFit"     with lines lt 1 lw 2.4 lc 1