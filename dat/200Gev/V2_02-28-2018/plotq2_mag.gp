reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
filename='./Q2_V2_02-28-2018.dat'
set out "V2.eps"
################kt=[0.15:0.65]
set xrange [0:3]
set yrange [3:10]
################
key_label=0.095
energy_label=3.3
kt_label = 1.3
################
set mytics   5
set mxtics   5
set ytics scale 1.
set xtics scale 1.

#set logscale x
#below is scale for axis tics
set xtics 0.5
set ytics 1.0
################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
################
#####DO NOT TOUCH####
set size    0.45,0.50
set origin -0.00,0.48
################

set key samplen  1.2
set key at 630,key_label
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  15"
set xlabel font "Times-Roman,  15" 
set ylabel font "Times-Roman,  15"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'V_{2}'
set ytics  offset 1,0.0
set ylabel offset 4,0.0
set label "|Q^2_{2}|"  at 1.5,-1.6    font "Times-Roman,  18"
 p (filename) every ::(4)::(4) u ($1*$1):($3*100):4 t"Q90-100%" with errorbars lt 1 lw 2 lc 7  pt 5  ps 1 ,\
  