reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#remove all .dat/.root/.anything from file name
name='926_gridsize005_noq4'
cent=0
filename=sprintf("%s%s%s", './dat/',name,'.dat')
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'q2dep_fixcent',cent,'.eps')
RPbins=4
set out "image.eps"
################
set xrange [0:99]
set yrange [0.5:4]
################
set mytics   5
set mxtics   5
set ytics scale 1.
set xtics scale 1.
#set logscale x
set xtics 20
set ytics 1
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
###############
################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins 2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is every command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
################
################
set size    0.45,0.50
set origin -0.00,0.48

set key samplen  1.2
set key at 145,2.3
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'R'
######DONT TOUCH#########
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
#########################
set label "Rside"       at 4,3.7    font "Times-Roman,  18" 

 p (filename) every ::0*RPbins:(cent):1*RPbins-1:(cent) u 6:12:($15*1) t"<.22>" with errorbars lt 1 lw 2 lc 1  pt 4  ps 1.75,\
   (filename) every ::1*RPbins:(cent):2*RPbins-1:(cent) u 6:12:($15*1)  t"K_{t}=<.29>"  with errorbars  lt 1 lw 2 lc 3  pt 6  ps 1.75,\
   (filename) every ::2*RPbins:(cent):3*RPbins-1:(cent) u 6:12:($15*1)  t"<.38>"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.75,\
   (filename) every ::3*RPbins:(cent):4*RPbins-1:(cent) u 6:12:($15*1)  t"K_{t}=<.48>"  with errorbars  lt 1 lw 2 lc 1  pt 7  ps 1.75

   #'q2dep_c3_k1'     u 6:12:($15*1)  t"<.29>"  with errorbars  lt 1 lw 2 lc 3  pt 6  ps 2,\
   #'q2dep_c3_k2'     u 6:12:($15*1)  t"<.38>"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #'q2dep_c3_k3'     u 6:12:($15*1)  t"<.48>"  with errorbars  lt 1 lw 2 lc 1  pt 7  ps 2


################
################
set size    0.45,0.50
set origin 0.30,0.48
unset key
unset label

set key samplen  1.2
set key at 1.4,-25
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xlabel 'Q_{2} %'
#set xla  offset 0,6.5

  set label "Rout"      at 5,3.8    font "Times-Roman,  18" 
  #set label "19 GeV"    at 5,4.4    font "Times-Roman,  18" 
  set label "Centrality=40-60%"       at 4,0.9    font "Times-Roman,  20" 
  set label 'Q_{2}%'    at 40,-0.2   font "Times-Roman,  24" 

 p (filename) every ::0*RPbins:(cent):1*RPbins-1:(cent)   u 6:13:($16*1) t"kt=<.22>" with errorbars lt 1 lw 2 lc 1  pt 4  ps 1.75,\
   (filename) every ::1*RPbins:(cent):2*RPbins-1:(cent)   u 6:13:($16*1)  t"<.29>"  with errorbars  lt 1 lw 2 lc 3  pt 6  ps 1.75,\
   (filename) every ::2*RPbins:(cent):3*RPbins-1:(cent)   u 6:13:($16*1)  t"<.38>"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.75,\
   (filename) every ::3*RPbins:(cent):4*RPbins-1:(cent)   u 6:13:($16*1)  t"<.48>"  with errorbars  lt 1 lw 2 lc 1  pt 7  ps 1.75
################
################
set size    0.45,0.50
set origin 0.6,0.48

unset label
unset key

set key samplen  1.2
set key at 1.4,-25
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '

  set label "Rlong"       at 75,3.7    font "Times-Roman,  18" 
  set label "19Gev"       at 75,3.3    font "Times-Roman,  18" 

 p (filename) every ::0*RPbins:(cent):1*RPbins-1:(cent)   u 6:14:($17*1) t"kt =<.22>" with  errorbars lt 1 lw 2 lc 1  pt 4  ps 1.75,\
   (filename) every ::1*RPbins:(cent):2*RPbins-1:(cent)   u 6:14:($17*1)  t"<.29>"    with errorbars  lt 1 lw 2 lc 3  pt 6  ps 1.75,\
   (filename) every ::2*RPbins:(cent):3*RPbins-1:(cent)   u 6:14:($17*1)  t"<.38>"    with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.75,\
   (filename) every ::3*RPbins:(cent):4*RPbins-1:(cent)   u 6:14:($17*1)  t"<.48>"    with errorbars  lt 1 lw 2 lc 1  pt 7  ps 1.75

###############
###############
unset multiplot
################