reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#filename='./dat/39gev_trackmerge5_1-02-2018/39gev_trackmerge5_1-02-2018.dat'
#filename6='./dat/39gev_trackmerge6_01-03-2018/39gev_trackmerge6_01-03-2018.dat'
#filename4='./dat/39gev_trackmerge7_01-03-2018/39gev_trackmerge7_01-03-2018.dat' #>100
#filename5='./dat/39gev_trackmerge8_01-05-2018/39gev_trackmerge8_01-05-2018.dat'
#filename5='./dat/fittest/39gev_trackmerge9_01-04-2018.dat'
#filename6='./dat/39gev_trackmerge6_01-03-2018/trackmerge6_nocoul/trackmerge6_nocoul.dat'
#filename5='./dat/39gev_trackmerge6_01-03-2018/trackmerge6_nocoul/trackmerge6_nocoul.dat'
#filename5='./dat/39gev_trackeff3_01-22-2018/trackeff3_nocoul_01-22-2018/trackeff3_nocoul_01-22-2018.dat'
#filename6='./dat/39gev_trackeff3_01-22-2018/39gev_trackeff3_01-22-2018.dat'
#filename6='./dat/54gev_hbt2_01-24-2018/nocoul/nocoul_54gev_hbt2_01-24-2018.dat'
#filename5='./dat/19gev_trackeff2_02-05-2018/19gev_trackeff2_02-05-2018.dat'
#filename6='./dat/39gev_trackeff4_noq_01-29-2018/39gev_trackeff4_noq_01-29-2018.dat'
#filename5='./dat/200gev_fullq2_02-12-2018/200gev_noq'
#filename5='./dat/39gev_trackeff7_01-31-2018/39gev_trackeff7_01-31-2018.dat'
#filename6='./dat/200gev_starmerge4_02-20-2018/nocoul/200gev_starmerge4_02-20-2018.dat'
#filename6='./dat/200gev_starmerge3_02-16-2018/nocoul/200gev_noq'
#filename5='./dat/200gev_fullq_02-12-2018/nocoul/200gev_noq'
#filename5='./dat/200Gev/200gev_Cumelant2_03-09-2018/noqkt'
#filename5='./dat/200Gev/y18Mar22Thur1556_resub/noqkt'
#filename5='./dat/200Gev/200gev_marchtest1_03-01-2018/200gev_marchtest1_coul_03-01-2018/200gev_marchtest1_coul_03-01-2018.dat'
#filename5='./dat/200Gev/y18Mar08Thu1535_6008/Coulomb/y18Mar08Thu1535_6008.dat'
#filename5='./dat/200Gev/y18Apr12Thu1940_6009_hbt/y18Apr12Thu1940_6009_hbt.dat'

#filename5='./dat/19Gev/19gev_noq_02-04-2018/19gev_noq_02-04-2018.dat'
filename5='./dat/200Gev/y18Mar08Thu1535_6008/y18Mar08Thu1535_6008.dat'
filename6='./dat/200Gev/y18Mar08Thu1535_6008/Coulomb/y18Mar08Thu1535_6008.dat'
filename7='./dat/200Gev/200gev_Cumelant2_03-09-2018/noqkt'
filename8='./dat/200Gev/y18Apr18Wed1426_hbt/noqkt'
filename3='./dat/STARDATA/star_data_200gev'
energy="200 Gev"
#filename5="./dat/39gev_trackeff6_01-30-2018/nocoul/39gev_trackeff6_01-30-2018.dat"
cent=0
#filename4="./dat/nocoulomb27gev_TOF.dat"
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'depkt_fix',cent,'multiq.eps')

set out "image.eps"
################kt=[0.15:0.65]
set xrange [0.15:0.67]
set yrange [3:7.5]
################
radii_label=7.4
energy_label=3.2
kt_label = 1.5
################
set mytics   5
set mxtics   5
set ytics scale 1.
set xtics scale 1.

#set logscale x
set xtics .1
set ytics 1
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
###############
################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
################
#####DO NOT TOUCH####
set size    0.45,0.50
set origin -0.00,0.48
################

set key samplen  1.2
set key at 0.6,radii_label
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'R'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set label "Rside"       at 0.16,radii_label    font "Times-Roman,  18" 
 p (filename3) every ::0:(cent)::(cent) u 3:4:5         t"PhysRevC.92.014904"   with errorbars lt 1 lw 2 lc 0  pt 13  ps 1,\
   (filename5) every ::0:(cent)::(cent) u 5:12:($15*1)  t"march8 nocoul"        with errorbars lt 1 lw 2 lc 1  pt 14  ps 1,\
   (filename6) every ::0:(cent)::(cent) u 5:12:($15*1)  t"march 8 w/coul"       with errorbars lt 1 lw 2 lc 2  pt 4  ps 1,\
   (filename7) every ::0:(cent)::(cent) u 5:12:($15*1)  t"Cumelant2 03-09-2018" with errorbars lt 1 lw 2 lc 3  pt 8  ps 1,\
   (filename8) every ::0:(cent)::(cent) u 5:12:($15*1)  t"April18"       with errorbars lt 1 lw 2 lc 4  pt 1  ps 1,\
   #(filename6) every ::0:(cent)::(cent) u 5:12:($15*1)  t"StarMerge3<0.1"        with errorbars lt 1 lw 2 lc 1  pt 7  ps 1,\
   #(filename4) every ::0:(cent)::(cent) u 5:12:($15*1)  t"Ave sep>100"        with errorbars lt 1 lw 2 lc 1  pt 12  ps 1,\
   #(filename) every ::0:(cent)::(cent) u 5:12:($15*1)   t"Ave sep>125"        with errorbars lt 1 lw 2 lc 1  pt 6  ps 1,\
   #(filename2) every ::0:(cent)::(cent) u 5:12:($15*1)  t"Ave sep>80"         with errorbars lt 1 lw 2 lc 1  pt 3  ps 1,\


################
################
set size    0.45,0.50
set origin 0.30,0.48
unset key
unset label

set key samplen  1.2
set key at 1.4,-5
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5

  set label "Rout"      at 0.16,radii_label   font "Times-Roman,  18" 
  set label (energy)    at 0.16,energy_label    font "Times-Roman,  18" 
  cent_label=sprintf("Centrality=%i%s",cent*10,'%')
  set label (cent_label)       at 0.16,(energy_label-0.37)    font "Times-Roman,  18" 
  set label 'K_{T}'     at 0.3,(kt_label)   font "Times-Roman,  24" 

 p  (filename3) every ::0:(cent)::(cent) u 3:6:7         t"Q20%" with errorbars lt 1 lw 2 lc 0  pt 13  ps 1,\
    (filename5) every ::0:(cent)::(cent) u 5:13:($16*1)  t"70"   with errorbars lt 1 lw 2 lc 1  pt 14  ps 1,\
    (filename6) every ::0:(cent)::(cent) u 5:13:($16*1)  t"70"   with errorbars lt 1 lw 2 lc 2  pt 4  ps 1,\
    (filename7) every ::0:(cent)::(cent) u 5:13:($16*1)  t"70"   with errorbars lt 1 lw 2 lc 3  pt 8  ps 1,\
    (filename8) every ::0:(cent)::(cent) u 5:13:($16*1)  t"70"   with errorbars lt 1 lw 2 lc 4  pt 1  ps 1,\
   #(filename6) every ::0:(cent)::(cent) u 5:13:($16*1)  t"Av90" with errorbars lt 1 lw 2 lc 1  pt 7  ps 1,\
   #(filename4) every ::0:(cent)::(cent) u 5:13:($16*1)  t"Q20%" with errorbars lt 1 lw 2 lc 1  pt 12  ps 1,\
   #(filename)  every ::0:(cent)::(cent) u 5:13:($16*1)  t"Q20%" with errorbars lt 1 lw 2 lc 1  pt 6  ps 1,\
   #(filename2) every ::0:(cent)::(cent) u 5:13:($16*1)  t"Q20%" with errorbars lt 1 lw 2 lc 1  pt 3  ps 1,\

   #(filename) u 5:13:($16*1)  t"Q40%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 1.5
   #(filename)  every RPbins::3:(cent)::(cent) u 5:13:($16*1)  t"Q80%"  with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   #(filename)  every RPbins::2:(cent)::(cent) u 5:13:($16*1)  t"Q60%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 1.5,\
   #(filename)  every RPbins::4:(cent)::(cent) u 5:13:($16*1)  t"Q35%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5
################
################
set size    0.45,0.50
set origin 0.6,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '


  set label "Rlong"       at 0.16,radii_label    font "Times-Roman,  18" 
  p (filename3) every ::0:(cent)::(cent) u 3:8:9  t"Q20%"       with errorbars lt 1 lw 2 lc 0  pt 13  ps 1,\
   (filename5) every ::0:(cent)::(cent) u 5:14:($17*1)  t"Ave"  with errorbars lt 1 lw 2 lc 1  pt 14  ps 1,\
   (filename6) every ::0:(cent)::(cent) u 5:14:($17*1)  t"Ave"  with errorbars lt 1 lw 2 lc 2  pt 4  ps 1,\
   (filename7) every ::0:(cent)::(cent) u 5:14:($17*1)  t"Ave"  with errorbars lt 1 lw 2 lc 3  pt 8  ps 1,\
   (filename8) every ::0:(cent)::(cent) u 5:14:($17*1)  t"Ave"  with errorbars lt 1 lw 2 lc 4  pt 1  ps 1,\
   #(filename6)  every ::0:(cent)::(cent) u 5:14:($17*1)  t"Ave " with errorbars lt 1 lw 2 lc 1  pt 7  ps 1,\
   #(filename4) every ::0:(cent)::(cent) u 5:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 1  pt 12  ps 1,\
   #(filename)  every ::0:(cent)::(cent) u 5:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 1  pt 6  ps 1,\
   #(filename2) every ::0:(cent)::(cent) u 5:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 1  pt 3  ps 1,\

   #(filename) u 5:14:($17*1)  t"Q40%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 1.5
   #(filename)  every RPbins::2:(cent)::(cent) u 5:14:($17*1)  t"Q60%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 1.5,\
   #(filename)  every RPbins::3:(cent)::(cent) u 5:14:($17*1)  t"Q80%"  with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   #(filename)  every RPbins::4:(cent)::(cent) u 5:14:($17*1)  t"Q35%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5

###############
###############
unset multiplot
################