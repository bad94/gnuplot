reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'q2dep_fixkt',KT,'.eps')
set out "image.eps"
################
set xrange [0.005:0.25]
set yrange [0.9:1.6]
################
set mytics   5
set mxtics   5
set ytics scale 1.0
set xtics scale 1.0
#set logscale x
#CHANGE HERE
set xtics 0.1
set ytics 0.2
#####################
#####################
set multiplot layout 0,2
#set xtics rotate
#set bmargin 5
#####################
#####################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
#####################
###DO NOT TOUCH######
set size    0.4,0.4
set origin -0.0,0.48
#####################
set key samplen  1.2
set key at 0.25,1.58
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"
#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0  ,0.5
set ytics  offset 0.1,0.0
set ylabel offset 2.9,0.0
#THESE ARE FOR SECECTION ONLY not changing the # of bins.
# cent goes from 0,1,2 for 19gev and kt goes from 0..3 and rpgoes from 0..4
#Rside=0,Rout=1,Rlong=2
set xtics  offset 0,0.5
set ylabel 'C(q)'
set ytics  offset 0.8,0.0
set ylabel offset 4,0.0

name= '39gev_trackmerge5_1-02-2018' #ave sep>125
#name1='39gev_trackmerge9_01-04-2018'#ave_sep>90
#name='39gev_trackeff6_01-30-2018'
#name2='39gev_trackmerge7_01-03-2018'#ave_sep>100
name2='39gev_trackmerge6_01-03-2018'#ave_sep>80

RType="side"
filename =sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
#filename1=sprintf("%s%s%s%s%s",'./dat/',name1,'/corr_',RType,'.dat')
filename2=sprintf("%s%s%s%s%s",'./dat/',name2,'/corr_',RType,'.dat')
#filename3=sprintf("%s%s%s%s%s",'./dat/',name3,'/corr_',RType,'.dat')

Rlabel = sprintf("%s%s",'R',RType)
#set label "Rside"       at 0.03,1.4    font "Times-Roman,  18" 
#set label "19Gev"       at .95,1.4     font "Times-Roman,  18"
cent=1
KT=0
RPbin=0
qselection=0
#Do not forget to remove uncessary spaces
#corr_block=(cent*5)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT;
#corr_block = (cent*16)+(KT*4)+RPbin #rpbin=4
#corr_block = (cent*16)+(KT*4)+RPbin
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename)  every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"track sep. cut" with errorbars lt 1 lw 2 lc 0  pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"cut+eff. correction" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
   #(filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"ave 90 /11" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
  # (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"ave 80 /11" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01

################################################
################################################
set size    0.4,0.4
set origin 0.21,0.48
unset key
unset label

set key samplen  1.2
set key at 1.4,-5
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT ;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename)  every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"ave 125/11" with errorbars lt 1 lw 2 lc 0  pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"ave 100/11" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
  # (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"ave 90/11" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
#   (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"ave 80/11" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01
################
################
set size    0.4,0.4
set origin 0.42,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2  lc 0  pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
 #  (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
 #  (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01
   
###################
###################
set size    0.4,0.4
set origin 0.63,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT ;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc  0 pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
 #  (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
 #  (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01

################################################
################################################
#################ROUT###########################
################################################
################################################
################################################
set size    0.4,0.4
set origin -0.0,0.233
#####################
unset label
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"
set ylabel 'C(q)'
RType="out"
filename =sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
#filename1=sprintf("%s%s%s%s%s",'./dat/',name1,'/corr_',RType,'.dat')
filename2=sprintf("%s%s%s%s%s",'./dat/',name2,'/corr_',RType,'.dat')
#filename3=sprintf("%s%s%s%s%s",'./dat/',name3,'/corr_',RType,'.dat')

Rlabel = sprintf("%s%s",'R',RType)
#set label "Rside"       at 0.03,1.4    font "Times-Roman,  18" 
#set label "19Gev"       at .95,1.4     font "Times-Roman,  18"
KT=0
#Do not forget to remove uncessary spaces
#corr_block=(cent*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin #rpbin=4
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin ; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc  0 pt 8  ps 1.5,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps 1,\
  # (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
  # (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps 0.1
 #############################################
 #############################################
 #############################################
set size    0.4,0.4
set origin 0.21,0.233
unset key
unset label

set key samplen  1.2
set key at 1.4,-5
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc  0 pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
  # (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
  # (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01
################
################
set size    0.4,0.4
set origin 0.42,0.233

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc  0 pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
  # (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
  # (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01

################
################
set size    0.4,0.4
set origin 0.63,0.233

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT ;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc  0 pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
  # (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
  # (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01


################################################
################################################
#################RLONG###########################
################################################
################################################################################################
###############################################
set size    0.4,0.4
set origin -0.0,-0.015
#####################
unset label
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"
set ylabel 'C(q)'
RType="long"
filename =sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
#filename1=sprintf("%s%s%s%s%s",'./dat/',name1,'/corr_',RType,'.dat')
filename2=sprintf("%s%s%s%s%s",'./dat/',name2,'/corr_',RType,'.dat')
#filename3=sprintf("%s%s%s%s%s",'./dat/',name3,'/corr_',RType,'.dat')

Rlabel = sprintf("%s%s",'R',RType)
#set label "Rside"       at 0.03,1.4    font "Times-Roman,  18" 
#set label "19Gev"       at .95,1.4     font "Times-Roman,  18"
KT=0
#Do not forget to remove uncessary spaces
#corr_block=(cent*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin #rpbin=4
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin ; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc  0 pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
 #  (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
 #  (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01
 #############################################
 ####################################################
 ##############################################
set size    0.4,0.4
set origin 0.21,-0.015
unset key
unset label

set key samplen  1.2
set key at 1.4,-5
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin; else corr_block = (cent*4)+KT ;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc  0 pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
 #  (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
 #  (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01
################
################
set size    0.4,0.4
set origin 0.42,-0.015

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin ; else corr_block = (cent*4)+KT;
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename)  every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 0  pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
  # (filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
 #  (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01
################
################
set size    0.4,0.4
set origin 0.63,-0.015

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*16)+(KT*4)+RPbin
if(qselection) corr_block = (cent*20)+(KT*5)+RPbin ; else corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename)  every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 0  pt 8  ps .01,\
   (filename2) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 1  pt 8  ps .01,\
   #(filename1) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 2  pt 8  ps .01,\
  # (filename3) every :::(corr_block)::(corr_block) u 1:2:($3*1)  t"CorrFunc" with errorbars lt 1 lw 2 lc 3  pt 8  ps .01