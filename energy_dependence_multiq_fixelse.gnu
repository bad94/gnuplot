reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
filename='./dat/200Gev/fullqselection200gev_11-10-2017.dat'
filename2='./dat/39Gev/39gev_newselection_11-13-2017.dat'
filename3='./dat/27Gev/27gev_CORRECT_QSELECTION_11-13-2017.dat'
filename4='./dat/19Gev/19gev_NEWQSELECTION_11-13-2017.dat'

filename5='./dat/19Gev/TOF19gev_10-27-2017.dat'
filename6='./dat/27Gev/noqselection27gev_10-19-2017.dat'
filename7='./dat/39Gev/39gev_NOQ_11-09-2017.dat'
filename8='./dat/200Gev/200gev_noqselection.dat'
cent=3
KT=0
RPbins=4
set out "image.eps"
################
set xrange [5:220]
set yrange [3:6]
##################
radii_label_y=5.8
energy_label_y=3
kt_label=0.7
#################
set mytics   5
set mxtics   5
set ytics scale 1.3
set xtics scale 1.

set logscale x
set xtics 10
set ytics 1
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
#########################
#########################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
#########################
#####DO NOT TOUCH########
set size    0.45,0.50
set origin -0.00,0.48
################
set key samplen  1.2
set key at 0.6,-6
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'R'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set label "Rside"       at 0.16,(radii_label_y)    font "Times-Roman,  18" 

p  (filename)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent)  u 1:12:($15*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent)  u 1:12:($15*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename2)  every ::(KT*5+0):(cent):(KT*5+0):(cent)           u 1:12:($15*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename2)  every ::(KT*5+3):(cent):(KT*5+3):(cent)           u 1:12:($15*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename3)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u 1:12:($15*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename3)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent) u 1:12:($15*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename4)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u 1:12:($15*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename4)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent) u 1:12:($15*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename5)  every ::(KT):(cent):(KT):(cent) u 1:12:($15*1)  t"Noq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename6)  every ::(KT):(cent):(KT):(cent) u 1:12:($15*1)  t"Noq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename7)  every ::(KT):(cent):(KT):(cent) u 1:12:($15*1)  t"Noq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename8)  every ::(KT):(cent):(KT):(cent) u 1:12:($15*1)  t"Noq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5
################
set size    0.45,0.50
set origin 0.30,0.48
unset key
unset label
#set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5

  set label "Rout"      at 0.16,(radii_label_y)   font "Times-Roman,  18" 
  set label "Kt=0."    at 0.16,(energy_label_y)    font "Times-Roman,  18" 
  # 0-5(0),  5-10(1),10-20(2),20-30(3),30-40(4),40-50(5),50-60(6),60-70(7)
  cent_label=sprintf("Centrality=%i%s",cent*10,'%')
  set label (cent_label)       at 0.16,(energy_label_y-.35)   font "Times-Roman,  18" 
  set label 'K_{T}'     at 0.33,(kt_label)    font "Times-Roman,  24" 

p  (filename)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent)  u 1:13:($16*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent)  u 1:13:($16*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename2)  every ::(KT*5+0):(cent):(KT*5+0):(cent)           u 1:13:($16*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename2)  every ::(KT*5+3):(cent):(KT*5+3):(cent)           u 1:13:($16*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename3)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u 1:13:($16*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename3)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent) u 1:13:($16*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename4)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u 1:13:($16*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename4)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent) u 1:13:($16*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename5)  every ::(KT):(cent):(KT):(cent) u 1:13:($16*1)  t"Noq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename6)  every ::(KT):(cent):(KT):(cent) u 1:13:($16*1)  t"Noq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename7)  every ::(KT):(cent):(KT):(cent) u 1:13:($16*1)  t"Noq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename8)  every ::(KT):(cent):(KT):(cent) u 1:13:($16*1)  t"Noq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5
######################
set size    0.45,0.50
set origin 0.6,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '


  set label "Rlong"       at 0.16,(radii_label_y)    font "Times-Roman,  18" 
 p  (filename)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u 1:14:($17*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent)  u 1:14:($17*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename2)  every ::(KT*5+0):(cent):(KT*5+0):(cent)           u 1:14:($17*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename2)  every ::(KT*5+3):(cent):(KT*5+3):(cent)           u 1:14:($17*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename3)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u 1:14:($17*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename3)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent) u 1:14:($17*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename4)  every ::(KT*RPbins+0):(cent):(KT*RPbins+0):(cent) u 1:14:($17*1)  t"Q20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1.5,\
   (filename4)  every ::(KT*RPbins+3):(cent):(KT*RPbins+3):(cent) u 1:14:($17*1)  t"Q80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 1.5,\
   (filename5)  every ::(KT):(cent):(KT):(cent) u 1:14:($17*1)  t"NOq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename6)  every ::(KT):(cent):(KT):(cent) u 1:14:($17*1)  t"NOq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename7)  every ::(KT):(cent):(KT):(cent) u 1:14:($17*1)  t"NOq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   (filename8)  every ::(KT):(cent):(KT):(cent) u 1:14:($17*1)  t"NOq"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5

###############
###############
unset multiplot
################

