reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'q2dep_fixkt',KT,'.eps')
set out "image.eps"
################
set xrange [0.005:0.19]
set yrange [0.93:1.39]
################
set mytics   5
set mxtics   5
set ytics scale 1.0
set xtics scale 1.0
#set logscale x
#CHANGE HERE
set xtics 0.05
set ytics 0.2
#####################
#####################
set multiplot layout 4,3
#set xtics rotate
#set bmargin 5
#####################
#####################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
#####################
###DO NOT TOUCH######
set size    0.4,0.4
set origin -0.0,0.48
#####################
set key samplen  1.2
set key at 0.15,1.35
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1"
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"
#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0  ,0.5
set ytics  offset 0.1,0.0
set ylabel offset 2.9,0.0
#THESE ARE FOR SECECTION ONLY not changing the # of bins.
# cent goes from 0,1,2 for 19gev and kt goes from 0..3 and rpgoes from 0..4
#Rside=0,Rout=1,Rlong=2
set xtics  offset 0,0.5
set ylabel 'C(q)'
set ytics  offset 0.8,0.0
set ylabel offset 4,0.0
name='./39gev_trackeff4_01-25-2018'
RType="side"
filename=sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
filename2=sprintf("%s%s%s%s%s",'./dat/',name,'/nocoul/corr_',RType,'.dat')
Rlabel = sprintf("%s%s",'R',RType)
#set label "Rside"       at 0.03,1.4    font "Times-Roman,  18" 
#set label "19Gev"       at .95,1.4     font "Times-Roman,  18"
cent=0
KT=0
RPbin=0
#Do not forget to remove uncessary spaces
#corr_block=(cent*5)+RPbin
#corr_block = (cent*20)+(KT*5)+RPbin #5 rpbin
#corr_block = (cent*16)+(KT*4)+RPbin #rpbin=4
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1

################################################
################################################
set size    0.4,0.4
set origin 0.21,0.48
unset key
unset label

set key samplen  1.2
set key at 1.4,-5
set key spacing 0.6

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
################
################
set size    0.4,0.4
set origin 0.42,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1

#################################################################
################################################################
set size    0.4,0.4
set origin 0.63,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
    ######################################
######################################
#################################################################
################################################################
################################ ROUT ################################
################################################################
################################################################
################################
################################
RType="out"
filename=sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
filename2=sprintf("%s%s%s%s%s",'./dat/',name,'/nocoul/corr_',RType,'.dat')
Rlabel = sprintf("%s%s",'R',RType)
set size    0.4,0.4
set origin 0.0,0.233

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  20"
set ylabel 'C(q)'

#set xlabel ' '
#RPbin=0
KT=0
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
#############################################
#############################################
set size    0.4,0.4
set origin 0.21,0.233

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
##################################################
##################################################
set size    0.4,0.4
set origin 0.42,0.233

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
   #######################################################3
   ########################################################
set size    0.4,0.4
set origin 0.63,0.233

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  1" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1

   ######################################
######################################
#################################################################
################################################################
################################ RLONG ################################
################################################################
################################################################
################################
################################
   RType="long"
filename=sprintf("%s%s%s%s%s",'./dat/',name,'/corr_',RType,'.dat')
filename2=sprintf("%s%s%s%s%s",'./dat/',name,'/nocoul/corr_',RType,'.dat')
Rlabel = sprintf("%s%s",'R',RType)

set size    0.4,0.4
set origin 0.0,-0.015

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  20" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  20"
set ylabel 'C(q)'

#set xlabel ' '
#RPbin=0
KT=0
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
#############################################
#############################################
set size    0.4,0.4
set origin 0.21,-0.015

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  20" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=1
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
##################################################
##################################################
set size    0.4,0.4
set origin 0.42,-0.015

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  20" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=2
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
   #######################################################3
   ########################################################
set size    0.4,0.4
set origin 0.63,-0.015

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  20" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
#RPbin=0
KT=3
#Do not forget to remove uncessary spaces
#corr_block = (cent*20)+(KT*5)+RPbin
#corr_block = (cent*16)+(KT*4)+RPbin
corr_block = (cent*4)+KT 
set label Rlabel       at 0.1,1.18    font "Times-Roman,  18"
#set label "19Gev"      at 0.14,1.18    font "Times-Roman,  18"
#set label sprintf("%s%i",'Kt=',KT)         at 0.14,1.16    font "Times-Roman,  16"
#set label sprintf("%s%i",'Cent=',cent)     at 0.14,1.14    font "Times-Roman,  16"
#set label sprintf("%s%i",'Q2bin=',RPbin)   at 0.14,1.12    font "Times-Roman,  16"
p  (filename2) every :::(corr_block)::(corr_block) u 1:4        t"No Coul"    with lines lt 1 lw 2   lc 0,\
   (filename) every :::(corr_block)::(corr_block) u 1:4        t"Coulomb"     with lines lt 1 lw 2.4 lc 1
