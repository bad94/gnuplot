reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
filename='./dat/200Gev/200gev_testkt_11-28-2017/200gev_testkt_11-28-2017.dat'
RPbins=5
cent=6
#filename2="./dat/fullnewpico27gev_10-19-2017.dat"
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'depkt_fix',cent,'multiq.eps')
set out "image.eps"
################
set xrange [0.15:0.7]
set yrange [0.5:1.7]
##################
radii_label_y=1.69
energy_label_y=0.7
kt_label=0.25
#################
set mytics   5
set mxtics   5
set ytics scale 1.3
set xtics scale 1.

#set logscale x
set xtics 0.19
set ytics 0.2
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
#########################
#########################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
#########################
#####DO NOT TOUCH########
set size    0.45,0.50
set origin -0.00,0.48
################

set key samplen  1.2
set key at 0.58,radii_label_y
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'R_{out}/R_{side}'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set arrow 2 from 0.15,1 to 0.7,1 nohead lt 0 lw 4 lc 0

p  (filename)  every RPbins::0:(cent)::(cent) u 5:($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($16/$13)*($16/$13)))  t"Q_{2} 0-20%" with errorbars  lt 1 lw 2 lc 3  pt 4  ps 1.5,\
   (filename)  every RPbins::4:(cent)::(cent) u 5:($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($16/$13)*($16/$13))) t"Q_{2} 80-100%" with errorbars  lt 1 lw 2 lc 7  pt 4  ps 1.5
 #  (filename2)  every RPbins::0:(cent)::(cent) u 5:12:($15*1) t"TpcQ20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 2,\
 #  (filename2)  every RPbins::3:(cent)::(cent) u 5:12:($15*1) t"TpcQ80%"       with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename)  every RPbins::4:(cent)::(cent) u 5:12:($15*1)  t"None"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2,\
   #(filename)  every RPbins::1:(cent)::(cent) u 5:12:($15*1)  t"Q40%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename)  every RPbins::2:(cent)::(cent) u 5:12:($15*1)  t"Q60%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
   #'q2dep_c3_k1'     u 6:12:($15*1)  t"<.29>"  with errorbars  lt 1 lw 2 lc 3  pt 6  ps 2,\
   #'q2dep_c3_k2'     u 6:12:($15*1)  t"<.38>"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #'q2dep_c3_k3'     u 6:12:($15*1)  t"<.48>"  with errorbars  lt 1 lw 2 lc 1  pt 7  ps 2
################
################
set size    0.45,0.50
set origin 0.30,0.48
unset key
unset label
#set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5

  set label "Rout"      at 0.16,(radii_label_y)   font "Times-Roman,  18" 
  set label "200 GeV"    at -0.45,(energy_label_y)    font "Times-Roman,  18" 
  # 0-5(0),  5-10(1),10-20(2),20-30(3),30-40(4),40-50(5),50-60(6),60-70(7)
  cent_label=sprintf("Centrality=%i%s",cent*10,'%')
  set label (cent_label)       at -0.45,(energy_label_y-.1)   font "Times-Roman,  18" 
  set label 'K_{T}'     at -0.2,(kt_label)    font "Times-Roman,  24" 

p  (filename)  every RPbins::0:(cent)::(cent) u 5:13:($16*1)  t"TPCQ20%" with errorbars lt 1 lw 2 lc 3  pt 4  ps 1.5,\
   (filename)  every RPbins::3:(cent)::(cent) u 5:13:($16*1)  t"TPCQ80%"  with errorbars  lt 1 lw 2 lc 7  pt 4  ps 1.5
   #(filename2)  every RPbins::0:(cent)::(cent) u 5:13:($16*1)  t"TpcQ20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   #(filename2)  every RPbins::3:(cent)::(cent) u 5:13:($16*1)  t"TpcQ80%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename)  every RPbins::4:(cent)::(cent) u 5:13:($16*1)  t"Qnone%"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
   #(filename)  every RPbins::1:(cent)::(cent) u 5:13:($16*1)  t"Q40%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename)  every RPbins::2:(cent)::(cent) u 5:13:($16*1)  t"Q60%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
################
################
set size    0.45,0.50
set origin 0.6,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '


  set label "Rlong"       at 0.16,(radii_label_y)    font "Times-Roman,  18" 
p  (filename)  every RPbins::0:(cent)::(cent) u 5:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 3  pt 4  ps 1.5,\
   (filename)  every RPbins::3:(cent)::(cent) u 5:14:($17*1)  t"Q80%"  with errorbars  lt 1 lw 2 lc 7  pt 4  ps 1.5
   #(filename2)  every RPbins::0:(cent)::(cent) u 5:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   #(filename2)  every RPbins::3:(cent)::(cent) u 5:14:($17*1)  t"Q80%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename)  every RPbins::4:(cent)::(cent) u 5:14:($17*1)  t"Qnone%"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
  #(filename)  every RPbins::2:(cent)::(cent) u 5:14:($17*1)  t"Q60%"  with errorbars  lt 1 lw 2 lc 1  pt 8  ps 2
   #(filename)  every RPbins::1:(cent)::(cent) u 5:14:($17*1)  t"Q80%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\


   #(filename)  every RPbins::4:(cent)::(cent) u 5:14:($17*1)  t"none%"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
   #(filename)  every RPbins::1:(cent)::(cent) u 5:14:($17*1)  t"Q40%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename)  every RPbins::2:(cent)::(cent) u 5:14:($17*1)  t"Q60%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\

###############
###############
unset multiplot
################

