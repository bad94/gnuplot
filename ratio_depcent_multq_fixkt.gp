reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
filename='./dat/200Gev/200gev_testkt_11-28-2017/200gev_testkt_11-28-2017.dat'
filename2="./dat/39Gev/39gev_11-20-2017.dat"
filename3="./dat/27Gev/27gev_kt2_12-03-2017/27gev_kt2_12-03-2017.dat"
filename4="./dat/19Gev/19gev_11-28-2017.dat"
RPbins=5
KT=0

#filename=sprintf("%s%s%s", './dat/',name,'.dat')
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'q2dep_fixkt',KT,'.eps')

set out "image.eps"
################
set xrange [-5:59]
set yrange [0.9:1.3]
################
radii_label_y=1.27
energy_label_y=0.97
################
set mytics   5
set mxtics   5
set ytics scale 1
set xtics scale 1

#set logscale x
set xtics 20
set ytics 0.2
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
###############
################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
################
#####DO NOT TOUCH####
set size    0.45,0.50
set origin -0.00,0.48
################

set key samplen  1.2
set key at 5,-6
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  18"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'Ratio Q_{2} (low/ high)'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set label "Rside"       at 0,radii_label_y    font "Times-Roman,  18"
set arrow 2 from -4,1 to 59,1 nohead lt 0 lw 4 lc 0

   p (filename) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:18:($21*1)    t"200gev"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
     (filename2) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:18:($21*1)    t"27gev"  with errorbars  lt 1 lw 2 lc 3  pt 8  ps 2,\
     (filename3) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:18:($21*1)    t"39gev"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
     (filename4) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:18:($21*1)    t"19gev%"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
   #p  (filename) every ::(KT*RPbins+0)::(KT*RPbins+0) u 2:12:($15*1)  t"Q20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:12:($15*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:12:($15*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\

   #'q2dep_c3_k1'     u 6:12:($15*1)  t"<.29>"  with errorbars  lt 1 lw 2 lc 3  pt 6  ps 2,\
   #'q2dep_c3_k2'     u 6:12:($15*1)  t"<.38>"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #'q2dep_c3_k3'     u 6:12:($15*1)  t"<.48>"  with errorbars  lt 1 lw 2 lc 1  pt 7  ps 2


################
################
set size    0.45,0.50
set origin 0.26,0.48
unset key
unset label

set key samplen  1.2
set key at -5,radii_label_y
set key spacing 1

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel " "
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5

  set label "Rout"      at 0,radii_label_y    font "Times-Roman,  18" 
  #set label "200 GeV"    at 45,energy_label_y    font "Times-Roman,  16" 
  set label "<Kt>=.30"       at 5,(energy_label_y-0.03)    font "Times-Roman,  16" 
  set label 'Centrality%'     at 18,(energy_label_y-0.15)    font "Times-Roman,  20" 
p   (filename)  every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:19:($22*1)    t"200gev"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
    (filename2) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:19:($22*1)    t"27gev"  with errorbars  lt 1 lw 2 lc 3  pt 8  ps 2,\
    (filename3) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:19:($22*1)    t"39gev"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
    (filename4) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:19:($22*1)    t"19gev"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
#p  (filename) every ::(KT*RPbins+0)::(KT*RPbins+0) u 2:13:($16*1)  t"Q20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:13:($16*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:13:($16*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
################
################
set size    0.45,0.50
set origin 0.52,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '
set label "Rlong"       at 0,radii_label_y    font "Times-Roman,  18" 
p   (filename) every ::(KT*RPbins+4)::(KT*RPbins+4)  u 2:20:($23*1)    t"Q90%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
    (filename2) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:20:($23*1)    t"Q90%"  with errorbars  lt 1 lw 2 lc 3  pt 8  ps 2,\
    (filename3) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:20:($23*1)    t"Q90%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
    (filename4) every ::(KT*RPbins+4)::(KT*RPbins+4)  u 2:20:($23*1)    t"Q80%"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
#p  (filename) every ::(KT*RPbins+0)::(KT*RPbins+0) u 2:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:14:($17*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:14:($17*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\

###############
###############
unset multiplot
################