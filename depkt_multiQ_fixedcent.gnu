reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#name='newpico_testqselection_11-13-2017'
#name='27gev_qdefchange_11-06-2017'
RPbins=5
cent=4
energy="39 Gev"
#filename='./dat/200gev_fullq_02-09-2018/200gev_fullq_02-09-2018.dat'
#filename='./dat/200gev_fullq_02-09-2018/200gev_highcents/200gev_fullq_02-09-2018.dat'
#filename='./dat/200Gev/y18Mar27Tue1916_pt/y18Mar27Tue1916_pt.dat'
#filename='./dat/200Gev/y18Apr02Mon937_hbt_test/y18Apr02Mon937_hbt_test.dat'
#filename='./dat/39gev_q2_02-01-2018/39gev_q2_02-01-2018.dat'
#filename='./dat/27gev_kt2_12-03-2017/27gev_kt2_12-03-2017.dat'
#filename2='./dat/27gev_noq_12-11-2017/27gev_noq_12-11-2017.dat'
#filename="./dat/200gev_testkt_11-28-2017/200gev_testkt_11-28-2017.dat"
#filename2='./dat/200gev_noqselection.dat'
#filename3='./dat/star_data_39gev'
#filename='./dat/200Gev/y18Apr18Wed1426_hbt/y18Apr18Wed1426_hbt.dat'
#filename='./dat/39Gev/39gev_q2_02-01-2018/39gev_q2_02-01-2018.dat'
#filename='./dat/39Gev/39gev_coultot2_11-29-2017/39gev_coultot2_11-29-2017.dat'
#filename='./dat/39Gev/39gev_1023.dat'
filename='./dat/39Gev/39gev_11-20-2017.dat'
filename_noq='./dat/39Gev/39gev_NOQ_11-09-2017.dat'
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'depkt_fix',cent,'multiq.eps')
set out "image.eps"
################
set xrange [0.15:0.69]
set yrange [1:6]
##################
radii_label_y=5.6
energy_label_y=1.9
kt_label=0.1
#################
set mytics   5
set mxtics   5
set ytics scale 1.3
set xtics scale 1.
#set logscale x
set xtics 0.19
set ytics 1
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
#########################
#########################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
#########################
#####DO NOT TOUCH########
set size    0.45,0.50
set origin -0.00,0.48
################
set key samplen  1.2
set key at 0.65,radii_label_y
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"
#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'R'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set label "Rside"       at 0.16,(radii_label_y)    font "Times-Roman,  18" 

p  (filename)  every RPbins::0:(cent)::(cent) u 5:12:($15*2)  t"Q_{2}=0-20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 1,\
 (filename_noq) every ::0:(cent)::(cent) u 5:12:($15*1)  t"No Q_{2} Selection"        with errorbars lt 1 lw 2 lc 0  pt 14  ps 1,\
   (filename)  every RPbins::4:(cent)::(cent) u 5:12:($15*2)  t"Q_{2}=80-100%"      with errorbars  lt 1 lw 2 lc 7  pt 10  ps 1,\
  (filename)  every RPbins::3:(cent)::(cent) u 5:12:($15*2)  t"Q_{2}=60-80%"      with errorbars  lt 1 lw 2 lc 1  pt 14  ps 1,\
   #(filename)  every RPbins::4:(cent)::(cent) u 5:12:($15*2)  t"Q_{2}=40-50%"      with errorbars  lt 1 lw 2 lc 7  pt 6  ps 1,\
   #(filename)  every RPbins::7:(cent)::(cent) u 5:12:($15*2)  t"Q_{2}=60-70%"      with errorbars  lt 1 lw 2 lc 7  pt 6  ps 1,\
   #(filename2) every ::0:(cent)::(cent)       u 5:12:($15*1)  t"None"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1,\
   #(filename3) every ::0:(cent)::(cent) u 3:4:5         t"Star Published" with errorbars lt 1 lw 2 lc 0  pt 12  ps 1,\
   #(filename2)  every ::0:(cent)::(cent) u 5:12:($15*1) t"TpcQ20%"       with errorbars  lt 1 lw 2 lc 3  pt 8  ps 2,\
   #(filename2)  every RPbins::4:(cent)::(cent) u 5:12:($15*1) t"TpcQ80%"       with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
   #(filename)  every RPbins::2:(cent)::(cent) u 5:12:($15*1)  t"Q_{2}=40-60%"       with errorbars  lt 1 lw 2 lc 2  pt 8  ps 1,\
   #(filename)  every RPbins::2:(cent)::(cent) u 5:12:($15*1)  t"Q60%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
   #'q2dep_c3_k1'     u 6:12:($15*1)  t"<.29>"  with errorbars  lt 1 lw 2 lc 3  pt 6  ps 2,\
   #'q2dep_c3_k2'     u 6:12:($15*1)  t"<.38>"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #'q2dep_c3_k3'     u 6:12:($15*1)  t"<.48>"  with errorbars  lt 1 lw 2 lc 1  pt 7  ps 2
################
################
set size    0.45,0.50
set origin 0.30,0.48
unset key
unset label
#set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5

  set label "Rout"      at 0.16,(radii_label_y)   font "Times-Roman,  18" 
  set label (energy)    at 0.18,(energy_label_y)    font "Times-Roman,  18" 
  # 0-5(0),  5-10(1),10-20(2),20-30(3),30-40(4),40-50(5),50-60(6),60-70(7)
  #cent_label=sprintf("Centrality=%i%s",cent*10,'%')
  cent_label="Centrality=30-40%"
  set label (cent_label)       at 0.18,(energy_label_y-.35)   font "Times-Roman,  18" 
  set label 'K_{T}'     at 0.33,(kt_label)    font "Times-Roman,  24" 

p  (filename)  every RPbins::0:(cent)::(cent) u 5:13:($16*2)  t"Q_{2}=30-40%"   with errorbars lt 1 lw 2 lc 3  pt 8  ps 1,\
   (filename_noq) every ::0:(cent)::(cent) u 5:13:($16*1)  t"No Q_{2} Selection"        with errorbars lt 1 lw 2 lc 0  pt 14  ps 1,\
   (filename)  every RPbins::4:(cent)::(cent) u 5:13:($16*2)  t"Q_{2}=80-100%"  with errorbars lt 1 lw 2 lc 7  pt 10  ps 1,\
   #(filename)  every RPbins::3:(cent)::(cent) u 5:13:($16*2)  t"Q_{2}=60-80%"   with errorbars lt 1 lw 2 lc 1  pt 14  ps 1,\
   #(filename)  every RPbins::4:(cent)::(cent) u 5:13:($16*2)  t"Q_{2}=40-50%"   with errorbars lt 1 lw 2 lc 7  pt 6  ps 1,\
   #(filename)  every RPbins::7:(cent)::(cent) u 5:13:($16*2)  t"Q_{2}=60-70%"   with errorbars lt 1 lw 2 lc 2  pt 4  ps 1,\
   #(filename2) every ::0:(cent)::(cent)       u 5:13:($16*1)  t"No Q"                with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1,\
   #(filename3) every ::0:(cent)::(cent) u 3:6:7  t"star Published" with errorbars lt 1 lw 2 lc 0  pt 12  ps 1,\
   #(filename2)  every RPbins::4:(cent)::(cent) u 5:13:($16*1)  t"Qnone%"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
   #(filename2)  every RPbins::0:(cent)::(cent) u 5:13:($16*1)  t"TpcQ20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   #(filename2)  every RPbins::3:(cent)::(cent) u 5:13:($16*1)  t"TpcQ80%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename)  every RPbins::1:(cent)::(cent) u 5:13:($16*1)  t"Q40%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
################
################
set size    0.45,0.50
set origin 0.6,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '

  set label "Rlong"       at 0.16,(radii_label_y)    font "Times-Roman,  18" 
p  (filename)  every RPbins::0:(cent)::(cent) u 5:14:($17*2)  t"Q20%"           with errorbars lt 1 lw 2 lc 3  pt 8  ps 1,\
(filename_noq) every ::0:(cent)::(cent) u 5:14:($17*1)  t"No Q_{2} Selection"        with errorbars lt 1 lw 2 lc 0  pt 14  ps 1,\
   (filename)  every RPbins::4:(cent)::(cent) u 5:14:($17*2)  t"Q60%"           with errorbars lt 1 lw 2 lc 7  pt 10  ps 1,\
  # (filename)  every RPbins::3:(cent)::(cent) u 5:14:($17*2)  t"Q60%"           with errorbars lt 1 lw 2 lc 1  pt 14  ps 1,\
   #(filename)  every RPbins::4:(cent)::(cent) u 5:14:($17*2)  t"Q_{2}=80-100%"  with errorbars lt 1 lw 2 lc 7  pt 6  ps 1,\
   #(filename)  every RPbins::7:(cent)::(cent) u 5:14:($17*2)  t"Q_{2}=60-80"    with errorbars lt 1 lw 2 lc 2  pt 4  ps 1,\
   #(filename2) every ::0:(cent)::(cent)       u 5:14:($17*1)  t"No Q"          with errorbars  lt 1 lw 2 lc 0  pt 8  ps 1.5,\
   #(filename3) every ::0:(cent)::(cent) u 3:8:9  t"star Published" with errorbars lt 1 lw 2 lc 0  pt 12  ps 1.5
   #(filename2)  every RPbins::0:(cent)::(cent) u 5:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   #(filename2)  every RPbins::3:(cent)::(cent) u 5:14:($17*1)  t"Q80%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename)  every RPbins::4:(cent)::(cent) u 5:14:($17*1)  t"Qnone%"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
   #(filename)  every RPbins::2:(cent)::(cent) u 5:14:($17*1)  t"Q60%"  with errorbars  lt 1 lw 2 lc 1  pt 8  ps 2
   #(filename)  every RPbins::1:(cent)::(cent) u 5:14:($17*1)  t"Q80%"  with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
   #(filename)  every RPbins::4:(cent)::(cent) u 5:14:($17*1)  t"none%"  with errorbars  lt 1 lw 2 lc 0  pt 8  ps 2
   #(filename)  every RPbins::1:(cent)::(cent) u 5:14:($17*1)  t"Q40%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\

###############
###############
unset multiplot
################

