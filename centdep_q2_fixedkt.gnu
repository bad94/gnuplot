reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#filename='./dat/11gev_kt0_11-30-2017/11gev_kt0_11-30-2017.dat'
RPbins=10
KT=2
#filename='./dat/39gev_q2_02-01-2018/39gev_q2_02-01-2018.dat'
filename='./dat/200gev_fullq_02-09-2018_add_1_4/200gev_fullq_02-09-2018_add_1_4.dat'
#######################################
#filename="./dat/39gev_11-20-2017.dat"
#filename2="./dat/27gev_kt2_12-03-2017/27gev_kt2_12-03-2017.dat"
#filename3='./dat/200gev_testkt_11-28-2017/200gev_testkt_11-28-2017.dat'
#filename4="./dat/19gev_11-28-2017.dat"
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'q2dep_fixkt',KT,'.eps')
set out "image.eps"
################
set xrange [-5:79]
set yrange [1.5:6.5]
################
radii_label_y=6.3
energy_label_y=2.4
################
set mytics   5
set mxtics   5
set ytics scale 1.
set xtics scale 1.

#set logscale x
set xtics 20
set ytics 1
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
###############
################
#ich	ict	ikt	irp	ktvalue	q2value	Norm1D	Lamda1D	Rinv	Norm3D	Lamda3D	Rside	Rout	Rlong	RsE	RoE	RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
################
#####DO NOT TOUCH####
set size    0.45,0.50
set origin -0.00,0.48
################

set key samplen  1.2
set key at 5,-6
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'R'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set label "Rside"       at 0,radii_label_y    font "Times-Roman,  18" 

p  (filename) every ::(KT*RPbins+0)::(KT*RPbins+0) u 2:12:($15*1)  t"Q_{2}=0-20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   (filename) every ::(KT*RPbins+8)::(KT*RPbins+8) u 2:12:($15*1)    t"Q_{2}=80-90%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename) every ::(KT*RPbins+3)::(KT*RPbins+3) u 2:12:($15*1)    t"Q80%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #(filename2) every ::(KT)::(KT) u 2:12:($15*1)  t"Q20%" with errorbars lt 1 lw 2 lc 0  pt 8  ps 2,\

   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:12:($15*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:12:($15*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\

   #'q2dep_c3_k1'     u 6:12:($15*1)  t"<.29>"  with errorbars  lt 1 lw 2 lc 3  pt 6  ps 2,\
   #'q2dep_c3_k2'     u 6:12:($15*1)  t"<.38>"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #'q2dep_c3_k3'     u 6:12:($15*1)  t"<.48>"  with errorbars  lt 1 lw 2 lc 1  pt 7  ps 2


################
################
set size    0.45,0.50
set origin 0.30,0.48
unset key
unset label

set key samplen  1.2
set key at -6,radii_label_y
set key spacing 1

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5

  set label "Rout"      at 0,radii_label_y    font "Times-Roman,  18" 
  set label "39 GeV"    at 0,energy_label_y    font "Times-Roman,  16" 
  set label "<Kt>=0.25"       at 0,(energy_label_y-0.5)    font "Times-Roman,  16" 
  set label 'Centrality%'     at 18,(energy_label_y-1.8)    font "Times-Roman,  24" 

p  (filename) every ::(KT*RPbins+0)::(KT*RPbins+0) u 2:13:($16*1)  t"Q_{2}=0-20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   (filename) every ::(KT*RPbins+8)::(KT*RPbins+8) u 2:13:($16*1)    t"Q_{2}=80-100%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename) every ::(KT*RPbins+3)::(KT*RPbins+3) u 2:13:($16*1)    t"Q80%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #(filename2) every ::(KT)::(KT) u 2:13:($16*1)  t"Q20%" with errorbars lt 1 lw 2 lc 0  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:13:($16*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:13:($16*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
################
################
set size    0.45,0.50
set origin 0.6,0.48

unset label
unset key


set key samplen  1.2
set key at 1.4,-5
set key spacing 0.5

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel ' '


  set label "Rlong"       at 0,radii_label_y    font "Times-Roman,  18" 
p  (filename) every ::(KT*RPbins+0)::(KT*RPbins+0) u 2:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
   (filename) every ::(KT*RPbins+8)::(KT*RPbins+8) u 2:14:($17*1)    t"Q90%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename) every ::(KT*RPbins+3)::(KT*RPbins+3) u 2:14:($17*1)    t"Q80%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #(filename2) every ::(KT)::(KT) u 2:14:($17*1)  t"Q20%" with errorbars lt 1 lw 2 lc 0  pt 8  ps 2
   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:14:($17*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:14:($17*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\

###############
###############
unset multiplot
################