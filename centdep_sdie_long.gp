reset
set term post eps enhanced color  24      
set term post landscape  "Times-Roman" 24
#filename='./dat/200gev_testkt_11-28-2017/200gev_testkt_11-28-2017.dat'
#filename='./dat/200gev_noqselection.dat'
#filename="./dat/39gev_11-20-2017.dat"
#filename='./dat/39gev_10fm_coultot_12-08-2017/39gev_10fm_coultot_12-08-2017.dat'
#filename="./dat/27gev_kt2_12-03-2017/27gev_kt2_12-03-2017.dat"
star='./dat/star_data_19gev'
RPbins=5
KT=2
#imagename = sprintf("%s%s%s%i%s",'./plots/',name,'q2dep_fixkt',KT,'.eps')

set out "image.eps"
################
set xrange [-5:59]
set yrange [0.9:1.4]
################
radii_label_y=1.38
energy_label_y=2.4
################
set mytics   5
set mxtics   5
set ytics scale 1.
set xtics scale 1.

#set logscale x
set xtics 20
set ytics 0.1
################
################
set multiplot layout 0,2 
#set xtics rotate
#set bmargin 5
###############
################
#ich  ict ikt irp ktvalue q2value Norm1D  Lamda1D Rinv  Norm3D  Lamda3D Rside Rout  Rlong RsE RoE RlE
# TO USE THIS GNUPLOT MACRO: 0) assumes 4 RP bins (2) assumes 4 kt bins 3)add empty lines to data at end of each centrality
#key iterator is the "every" command
#every I:J:K:L:M:N
#I      Line increment
#J      Data block increment
#K      The first line
#L      The first data block
#M      The last line
#N      The last data block 
################
#####DO NOT TOUCH####
set size    0.45,0.50
set origin -0.00,0.48
################

set key samplen  1.2
set key at 35,radii_label_y
set key spacing 1.0
#NOTE:FONT SIZE OF 1 is the same as invisible
set key    font "Times-Roman,  15"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  20"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  22"

#set xlabel 'Q_2'
#set xlabel offset 0,0.1
set xtics  offset 0,0.5
set ylabel 'R_{out}/R_{side}'
set ytics  offset 0.7,0.0
set ylabel offset 2.9,0.0
set label "19 GeV"    at 10,0.95    font "Times-Roman,  16" 
set label "<Kt>=.30"       at 10,0.92    font "Times-Roman,  16" 
set label 'Centrality%'     at 15,0.8    font "Times-Roman,  24" 
#(($12/$14)*sqrt(($15/$12)*($15/$12)+($16/$14)*($16/$14)))
set arrow 2 from -5,1 to 59,1 nohead lt 0 lw 4 lc 0
 p  (star)    every ::(KT)::(KT) u 1:($6/$4):5 t"Star"  with errorbars lt 1 lw 2 lc 0  pt 7  ps 2
#p  (filename) every ::(KT)::(KT) u 2:($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13))) t"Q_{2} None"  with errorbars lt 1 lw 2 lc 0  pt 8  ps 2,\
#p  (filename) every ::(KT*RPbins+0)::(KT*RPbins+0) u 2:($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13))) t"Q_{2} 0-20%"  with errorbars lt 1 lw 2 lc 3  pt 8  ps 2,\
#   (filename) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:($13/$12):(($13/$12)*sqrt(($15/$12)*($15/$12)+($17/$13)*($17/$13))) t"Q_{2} 0-100%"   with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2
   #(filename) every ::(KT*RPbins+1)::(KT*RPbins+1) u 2:12:($15*1)    t"Q30%" with errorbars lt 1 lw 2 lc 1  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+2)::(KT*RPbins+2) u 2:12:($15*1)    t"Q50%" with errorbars  lt 1 lw 2 lc 2  pt 8  ps 2,\
   #(filename) every ::(KT*RPbins+4)::(KT*RPbins+4) u 2:12:($15*1)    t"Q90%"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2

   #'q2dep_c3_k1'     u 6:12:($15*1)  t"<.29>"  with errorbars  lt 1 lw 2 lc 3  pt 6  ps 2,\
   #'q2dep_c3_k2'     u 6:12:($15*1)  t"<.38>"  with errorbars  lt 1 lw 2 lc 7  pt 8  ps 2,\
   #'q2dep_c3_k3'     u 6:12:($15*1)  t"<.48>"  with errorbars  lt 1 lw 2 lc 1  pt 7  ps 2


################
################
set size    0.45,0.50
set origin 0.30,0.48
unset key
unset label

set key samplen  1.2
set key at -6,radii_label_y
set key spacing 1

set key    font "Times-Roman,  13"
set xtics  font "Times-Roman,  15" 
set ytics  font "Times-Roman,  1"
set xlabel font "Times-Roman,  1" 
set ylabel font "Times-Roman,  1"
#set xlabel 'p_{T}(GeV/c)'
#set xla  offset 0,6.5

  set label "Rout"      at 0,radii_label_y    font "Times-Roman,  18" 
  set label "<Kt>=.30"       at 0,(energy_label_y-0.5)    font "Times-Roman,  16" 
  set label 'Centrality%'     at 18,(energy_label_y-1.8)    font "Times-Roman,  24" 

